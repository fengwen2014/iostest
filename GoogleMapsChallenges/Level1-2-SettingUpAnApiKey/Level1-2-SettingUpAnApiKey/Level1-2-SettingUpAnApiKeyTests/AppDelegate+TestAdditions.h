#import "AppDelegate.h"

@interface AppDelegate (TestAdditions)

@property (strong, nonatomic) NSString *apiKey;
@property (strong, nonatomic) NSNumber *apiKeySuccess;

@end
