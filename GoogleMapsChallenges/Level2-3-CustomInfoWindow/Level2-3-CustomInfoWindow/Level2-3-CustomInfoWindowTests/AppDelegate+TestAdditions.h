#import "AppDelegate.h"

@interface AppDelegate (TestAdditions)

@property (strong, nonatomic) NSArray *markers;
@property (strong, nonatomic) NSArray *imageNames;

@end
