#import "AppDelegate.h"

@class GMSCameraPosition;

@interface AppDelegate (TestAdditions)

@property (strong, nonatomic) NSArray *markers;
@property (strong, nonatomic) NSNumber *inTheShopCount;

@end
