#import <GoogleMaps/GoogleMaps.h>

@interface TruckMarker : GMSMarker

@property (nonatomic, copy) NSString *objectID;

// TODO: Add a BOOL property named inTheShop
@property (nonatomic, assign) BOOL inTheShop;

@end
