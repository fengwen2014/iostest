#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface StreetViewVC : UIViewController

@property (assign, nonatomic) CLLocationCoordinate2D coordinate;

@end
